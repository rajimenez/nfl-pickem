import React from 'react';
import Game from './Game';
import { get as getSelections } from 'api/Selections';
import { useAuth0 } from 'react-auth0-wrapper';

export interface Game {
  AwayTeamLocation: string;
  AwayTeamName: string;
  AwayTeamShortName: string;
  HomeTeamLocation: string;
  HomeTeamName: string;
  HomeTeamShortName: string;
  HomeID: string;
  AwayID: string;
  AwayTeamScore?: number;
  HomeTeamScore?: number;
  TimePlayed?: string;
  UUID: string;
}

interface Props {
  games: Game[];
}

export interface Selected {
  [gameID: string]: { gameID: string; teamID: string; teamName: string };
}

function GamesTable({ games }: Props) {
  const [selections, setSelections] = React.useState<Selected>({});
  const { getTokenSilently } = useAuth0();
  React.useEffect(() => {
    (async () => {
      const token = await getTokenSilently();
      const data = await getSelections(token);
      setSelections(data);
    })();
  }, []);
  return (
    <>
      {games.map(g => (
        <Game
          key={g.UUID}
          {...g}
          dbSelected={selections[g.UUID] && selections[g.UUID].teamName}
        />
      ))}
    </>
  );
}

export default GamesTable;
