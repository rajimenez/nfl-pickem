module.exports = permissionRequired => {
  return (req, res, next) => {
    const error = res => {
      const err_message = 'Insufficient permissions';

      res.append(
        'WWW-Authenticate',
        `Bearer permission="${permissionRequired}", error="${err_message}"`
      );
      res.status(403).send(err_message);
    };

    const hasPermissions = req.user.permissions.includes(permissionRequired);
    return hasPermissions ? next() : error(res);
  };
};
