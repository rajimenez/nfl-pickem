import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';

const colorTheme = {
  colors: {
    primary: '#0091D5',
    secondary: '#EA6A47',
    third: '#A5D8DD',
    fourth: '#1C4E80',
    fifth: '#7E909A',
    sixth: '#F1F1F1',
    text: '#202020',
  },
};

export default makeStyles((theme: Theme) => ({
  page: {
    maxWidth: '1250px',
    margin: 0,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  weekCard: {
    margin: '0.3em 2em 2em 2em',
    padding: '1em 2em',
  },
  loginCard: {
    maxWidth: '50vw',
    margin: 'auto',
    padding: '3em 2em',
    textAlign: 'center',
  },
  loginButton: {
    margin: '0em 0.5em',
  },
  game: {
    margin: '1em',
    transition: 'opacity .50s linear;',
  },
  showGame: {
    opacity: 1,
  },
  hideGame: {
    opacity: 0,
  },
  saveState: {
    transition: 'opacity 500ms 0ms linear, height 500ms 0ms linear;',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  saveStateHide: {
    opacity: 0,
    margin: '0px',
    padding: '0px',
    height: '0px',
  },
  saveStateShow: {
    opacity: 1,
    height: '12px',
  },
  saveMessage: {
    fontSize: '1em',
    color: '#ccc',
    textTransform: 'uppercase',
  },
  submitButton: {
    margin: '0px 0.5em',
  },
  submitButtonGrid: {
    alignSelf: 'flex-end',
  },
  teamCompleted: {
    padding: '1em',
    border: '1px solid',
    borderColor: '#fff',
    textTransform: 'uppercase',
  },
  teamCompletedCorrect: {
    padding: '1em',
    border: '1px solid',
    borderColor: '#fff',
    backgroundColor: '#90ee90',
    textTransform: 'uppercase',
  },
  teamCompletedWrong: {
    padding: '1em',
    border: '1px solid',
    borderColor: '#fff',
    backgroundColor: '#ee9090',
    textTransform: 'uppercase',
  },
  teamCompletedSelected: {
    backgroundColor: colorTheme.colors.third,
  },
  team: {
    padding: '1em',
    cursor: 'pointer',
    border: '1px solid',
    borderColor: '#fff',
    textTransform: 'uppercase',
    '&:hover': {
      border: '1px outset',
      backgroundColor: colorTheme.colors.sixth,
    },
    '&:active': {
      border: '1px inset',
    },
  },
  homeTeam: {
    textAlign: 'end',
  },
  centerOfTeams: {
    textAlign: 'center',
    padding: '0.85em',
    color: '#ccc',
    fontSize: '0.75em',
    fontStyle: 'italic',
  },
  timePlayed: {
    color: '#ccc',
    fontSize: '0.75em',
    fontStyle: 'italic',
  },
  teamSelected: {
    backgroundColor: colorTheme.colors.third,
    '&:hover': {
      border: '1px outset',
      backgroundColor: colorTheme.colors.third,
    },
  },
  pageTitle: {
    padding: '0.3em 1em',
  },
  navbarTitle: {
    color: '#fff',
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'none',
    },
  },
  navbarTab: {
    color: '#fff',
  },
  navbarSpacing: {
    minWidth: '2em',
  },
  rightNavbarTab: {
    color: '#fff',
    marginLeft: 'auto',
  },
  adminTableRow: {
    border: 'solid 1px #ccc',
    paddingTop: '0.25em',
    paddingBottom: '0.25em',
    '&:nth-child(even)': {
      backgroundColor: '#ccc',
    },
  },
}));
