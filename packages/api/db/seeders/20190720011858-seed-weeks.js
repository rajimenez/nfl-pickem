const path = require('path');
const fs = require('fs');

'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    const filePath = path.join(__dirname, 'seed_weeks.sql');
    const sql = fs.readFileSync(filePath).toString();
    return queryInterface.sequelize.query(sql);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.sequelize.query('TRUNCATE TABLE weeks;');
  }
};
