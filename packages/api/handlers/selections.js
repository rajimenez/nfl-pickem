const db = require('../db/models/index');
const { selection, team } = db;

const selectionsHandler = async (req, res) => {
  const userID = req.user.sub;
  return res.json(
    await selection.findAll({ where: { user_id: userID }, include: [team] }),
  );
};

module.exports = selectionsHandler;
