import React from 'react';
import WeekSelect from 'components/week-select/WeekSelect';
import { get } from 'api/Games';
import GamesTable, { Game, Selected } from 'components/games-table/Games';
import { useAuth0 } from 'react-auth0-wrapper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import useStyles from 'styles/styles';
import Grid from '@material-ui/core/Grid';
import isAfter from 'date-fns/isAfter';

export const getCurrentWeek = () => {
  const weekStarts = [
    new Date(2019, 8, 5),
    new Date(2019, 8, 12),
    new Date(2019, 8, 19),
    new Date(2019, 8, 26),
    new Date(2019, 9, 3),
    new Date(2019, 9, 10),
    new Date(2019, 9, 17),
    new Date(2019, 9, 24),
    new Date(2019, 9, 31),
    new Date(2019, 10, 7),
    new Date(2019, 10, 14),
    new Date(2019, 10, 21),
    new Date(2019, 10, 28),
    new Date(2019, 11, 5),
    new Date(2019, 11, 12),
    new Date(2019, 11, 19),
    new Date(2019, 11, 26),
  ];

  const date = new Date();
  let count = 1;
  while (isAfter(date, weekStarts[count])) {
    count += 1;
    if (count === 17) {
      return 17;
    }
  }

  return count;
};

export default () => {
  const [week, setWeek] = React.useState(getCurrentWeek());
  const [games, setGames] = React.useState<Game[]>([]);
  const { getTokenSilently } = useAuth0();
  const classes = useStyles();

  React.useEffect(() => {
    const fetchGames = async () => {
      const token = await getTokenSilently();

      const g = await get(week, token);
      setGames(g);
    };
    fetchGames();
  }, [week]);

  const weekSelectAndButtons = () => {
    return (
      <Grid container>
        <Grid item xs={6}>
          <WeekSelect value={week} onChange={setWeek} />
        </Grid>
      </Grid>
    );
  };
  return (
    <div className={classes.page}>
      <div>
        <Typography variant="h4" className={classes.pageTitle}>
          Week {week}
        </Typography>
        <Paper className={classes.weekCard}>
          {weekSelectAndButtons()}
          <GamesTable games={games} />
        </Paper>
      </div>
    </div>
  );
};
