import React from 'react';
import { Route } from 'react-router-dom';
import Home from 'pages/home/Home';
import League from 'pages/league/League';
import Week from 'pages/week/Week';
import AdminWeek from 'pages/admin/Week';
import { useAuth0, checkPermissions } from '../../react-auth0-wrapper';
import Login from 'pages/login/Login';

export default () => {
  const { isAuthenticated, loading, token } = useAuth0();
  if (loading) {
    return <div />;
  }
  if (!isAuthenticated) {
    return <Login />;
  }
  const canCreateGames = checkPermissions(token, 'create:games');
  return (
    <>
      <Route exact path="/" component={Home} />
      <Route path="/league" component={League} />
      <Route path="/week" component={Week} />
      {canCreateGames && <Route path="/admin/week" component={AdminWeek} />}
    </>
  );
};
