import React from 'react';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';

interface Props {
  value: number;
  onChange: (newValue: number) => void;
}

// leaving just in case the buttons don't work out!
// export default ({ value, onChange }: Props) => (
//   <Grid container spacing={1} direction="column">
//     <Grid item xs={12}>
//       <InputLabel shrink htmlFor="select-week">
//         Select A Week
//       </InputLabel>
//       <Select
//         fullWidth
//         aria-label="full select"
//         input={<Input name="week" id="select-week" />}
//         name="week"
//         value={value}
//         onChange={e => onChange(e.target.value as number)}
//       >
//         <MenuItem value={1}>Week 1</MenuItem>
//         <MenuItem value={2}>Week 2</MenuItem>
//         <MenuItem value={3}>Week 3</MenuItem>
//         <MenuItem value={4}>Week 4</MenuItem>
//         <MenuItem value={5}>Week 5</MenuItem>
//         <MenuItem value={6}>Week 6</MenuItem>
//         <MenuItem value={7}>Week 7</MenuItem>
//         <MenuItem value={8}>Week 8</MenuItem>
//         <MenuItem value={9}>Week 9</MenuItem>
//         <MenuItem value={10}>Week 10</MenuItem>
//         <MenuItem value={11}>Week 11</MenuItem>
//         <MenuItem value={12}>Week 12</MenuItem>
//         <MenuItem value={13}>Week 13</MenuItem>
//         <MenuItem value={14}>Week 14</MenuItem>
//         <MenuItem value={15}>Week 15</MenuItem>
//         <MenuItem value={16}>Week 16</MenuItem>
//         <MenuItem value={17}>Week 17</MenuItem>
//       </Select>
//     </Grid>
//   </Grid>
// );
export default ({ value, onChange }: Props) => {
  const largeGroup = (
    <ButtonGroup size="small" id="select-week">
      <Button onClick={() => onChange(1)}>{'<<'}</Button>
      <Button disabled={value === 1} onClick={() => onChange(value - 1)}>
        {'<'}
      </Button>
      {value > 2 && (
        <Button onClick={() => onChange(value - 2)}>{value - 2}</Button>
      )}
      {value > 1 && (
        <Button onClick={() => onChange(value - 1)}>{value - 1}</Button>
      )}
      <Button disabled>{value}</Button>
      {value < 17 && (
        <Button onClick={() => onChange(value + 1)}>{value + 1}</Button>
      )}
      {value < 16 && (
        <Button onClick={() => onChange(value + 2)}>{value + 2}</Button>
      )}
      <Button disabled={value === 17} onClick={() => onChange(value + 1)}>
        {'>'}
      </Button>
      <Button onClick={() => onChange(17)}>{'>>'}</Button>
    </ButtonGroup>
  );

  const smallGroup = (
    <ButtonGroup size="small" id="select-week">
      <Button onClick={() => onChange(1)}>{'<<'}</Button>
      <Button disabled={value === 1} onClick={() => onChange(value - 1)}>
        {'<'}
      </Button>
      <Button disabled={value === 17} onClick={() => onChange(value + 1)}>
        {'>'}
      </Button>
      <Button onClick={() => onChange(17)}>{'>>'}</Button>
    </ButtonGroup>
  );

  return (
    <>
      <Hidden smDown>{largeGroup}</Hidden>
      <Hidden mdUp>{smallGroup}</Hidden>
    </>
  );
};
