import React from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import useStyles from 'styles/styles';
import parseDate from 'date-fns/parseISO';
import formatDate from 'date-fns/format';
import { Game } from './Games';
import Fade from '@material-ui/core/Fade';
import { Typography } from '@material-ui/core';
import { post as postSelection } from 'api/Selections';
import { useAuth0 } from 'react-auth0-wrapper';
import isBefore from 'date-fns/isBefore';

interface IProps extends Game {
  dbSelected?: string;
}

enum SavingState {
  NONE,
  SAVING,
  SAVED,
  ERROR,
}

export default ({
  UUID,
  dbSelected,
  AwayTeamName,
  AwayTeamLocation,
  AwayTeamScore,
  TimePlayed,
  HomeTeamName,
  HomeTeamLocation,
  HomeTeamScore,
  HomeID,
  AwayID,
}: IProps) => {
  const [selected, setSelected] = React.useState(dbSelected);
  const [saved, setSaved] = React.useState(0);
  const classes = useStyles();
  const { getTokenSilently } = useAuth0();
  const gameHasStarted =
    TimePlayed && isBefore(new Date(TimePlayed), new Date());

  const handleSelected = async (newSelected: string) => {
    if (selected === newSelected) {
      return;
    }

    if (gameHasStarted) {
      return;
    }

    setSaved(SavingState.SAVING);
    const selectedID = HomeTeamName === newSelected ? HomeID : AwayID;
    const token = await getTokenSilently();
    const response = await postSelection(
      { selections: { [UUID]: selectedID } },
      token,
    );
    if (response.success === false) {
      console.error(response.error);
      setSaved(SavingState.ERROR);
      return;
    }
    setSaved(SavingState.SAVED);
    setSelected(newSelected);
    setTimeout(() => setSaved(SavingState.NONE), 2000);
  };

  const getCSSClasses = (homeAway: 'home' | 'away') => {
    const css: string[] = [];
    const hasWinner =
      AwayTeamScore != null &&
      HomeTeamScore != null &&
      AwayTeamScore !== HomeTeamScore;

    if (gameHasStarted) {
      css.push(classes.teamCompleted);
    } else {
      css.push(classes.team);
    }

    if (homeAway === 'home') {
      css.push(classes.homeTeam);
    }

    if (hasWinner) {
      // @ts-ignore
      const awayWon = AwayTeamScore > HomeTeamScore;
      if (awayWon && homeAway === 'away' && selected === AwayTeamName) {
        css.push(classes.teamCompletedCorrect);
      } else if (!awayWon && homeAway === 'home' && selected === HomeTeamName) {
        css.push(classes.teamCompletedCorrect);
      } else if (homeAway === 'away' && selected === AwayTeamName) {
        css.push(classes.teamCompletedWrong);
      } else if (homeAway === 'home' && selected === HomeTeamName) {
        css.push(classes.teamCompletedWrong);
      }
    } else {
      if (selected === AwayTeamName) {
        if (homeAway === 'away') {
          if (gameHasStarted) {
            css.push(classes.teamCompletedSelected);
          } else {
            css.push(classes.teamSelected);
          }
        }
      }
      if (selected === HomeTeamName) {
        if (homeAway === 'home') {
          if (gameHasStarted) {
            css.push(classes.teamCompletedSelected);
          } else {
            css.push(classes.teamSelected);
          }
        }
      }
    }
    return css;
  };

  return (
    <Grid container>
      <Grid item xs>
        <Grid
          container
          onClick={() => handleSelected(AwayTeamName)}
          className={getCSSClasses('away').join(' ')}
        >
          <Hidden xsDown>
            <Grid item sm={5}>
              {AwayTeamLocation}
            </Grid>
          </Hidden>
          <Grid item sm={5} xs={10}>
            {AwayTeamName}
          </Grid>
          <Grid item sm={2}>
            {AwayTeamScore}
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={2} sm={1} className={classes.centerOfTeams}>
        <div
          className={`${classes.saveState} ${
            saved === SavingState.NONE
              ? classes.saveStateShow
              : classes.saveStateHide
          }`}
        >
          <div className={classes.timePlayed}>
            {TimePlayed && formatDate(parseDate(TimePlayed), 'EEEE hh:mm a')}
          </div>
        </div>
        <div
          className={`${classes.saveState} ${
            saved === SavingState.NONE
              ? classes.saveStateHide
              : classes.saveStateShow
          }`}
        >
          <div className={classes.saveMessage}>
            {saved === SavingState.SAVING
              ? 'saving'
              : saved === SavingState.ERROR
              ? 'error'
              : 'saved'}
          </div>
        </div>
      </Grid>
      <Grid item xs>
        <Grid
          container
          onClick={() => handleSelected(HomeTeamName)}
          className={getCSSClasses('home').join(' ')}
        >
          <Grid item sm={2}>
            {HomeTeamScore}
          </Grid>
          <Grid item sm={5} xs={10}>
            {HomeTeamName}
          </Grid>
          <Hidden xsDown>
            <Grid item sm={5}>
              {HomeTeamLocation}
            </Grid>
          </Hidden>
        </Grid>
      </Grid>
    </Grid>
  );
};
