require('dotenv').config();
const jwt = require('express-jwt');
const hasPermission = require('./permission-middleware');
const jwksRsa = require('jwks-rsa');
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const postGames = require('./handlers/postGames');
const games = require('./handlers/games');
const selections = require('./handlers/selections');
const postSelections = require('./handlers/postSelections');

app.use(cors());
app.use(bodyParser.json());

const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://c-and-ct.auth0.com/.well-known/jwks.json`,
  }),

  audience: 'https://nfl-pickem.site',
  issuer: 'https://c-and-ct.auth0.com/',
  algorithms: ['RS256'],
});

const canCreateGames = hasPermission('create:games');

app.get('/games', checkJwt, games);
app.get('/selections', checkJwt, selections);
app.post('/selections', checkJwt, postSelections);
app.post('/games', checkJwt, canCreateGames, postGames);

app.listen(8080);
