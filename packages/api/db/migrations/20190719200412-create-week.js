'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('weeks', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal('gen_random_uuid()')
      },
      week: {
        type: Sequelize.INTEGER
      },
      year: {
        type: Sequelize.INTEGER
      },
    }, {
      uniqueKeys: {
        unique_week: {
          fields: ['week', 'year'],
        },
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('weeks');
  }
};
