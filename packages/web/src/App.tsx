import React from 'react';
import './App.css';
import Navbar from 'components/navbar/Navbar';
import Routes from 'components/routes/Routes';

const App: React.FC = () => {
  return (
    <>
      <Navbar />
      <Routes />
    </>
  );
};

export default App;
