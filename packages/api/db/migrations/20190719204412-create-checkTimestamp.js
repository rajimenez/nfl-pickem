'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
    create or replace function checkSelectionTime()
      returns trigger as
    $$
    BEGIN
        if ((select time_played from games where id = new.game_id) < now()) then
            raise exception 'cannot set user picks if game has been started';
        end if;
        return new;
    end;
    $$ language plpgsql;

    create trigger checkTimestamp before insert or update on selections for each row execute procedure checkSelectionTime();
    `);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
      DROP TRIGGER checkTimestamp;
      DROP FUNCTION checkSelectionTime;
    `);
  },
};
