import axios from 'axios';

export interface Selections {
  [gameID: string]: Selection;
}

export interface Selection {
  gameID: string;
  teamID: string;
  teamName: string;
}

export const get = async (token: string) => {
  const { data } = await axios.get('http://localhost:8080/selections', {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const selections: Selections = {};

  data.forEach((s: any) => {
    selections[s.game_id] = {
      gameID: s.game_id,
      teamID: s.team_id,
      teamName: s.team.name,
    };
  });
  return selections;
};

export const post = async (
  selection: { selections: { [gameID: string]: string } },
  token: string,
) => {
  const { data } = await axios.post(
    'http://localhost:8080/selections',
    selection,
    { headers: { Authorization: `Bearer ${token}` } },
  );

  console.log(data);
  return data;
};
