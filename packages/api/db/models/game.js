'use strict';
module.exports = (sequelize, DataTypes) => {
  const game = sequelize.define('game', {
    home_team: DataTypes.UUID,
    away_team: DataTypes.UUID,
    home_score: DataTypes.INTEGER,
    away_score: DataTypes.INTEGER,
    time_played: DataTypes.DATE,
    week_played: DataTypes.UUID,
  }, {
    underscored: true,
    timestamps: false,
  });
  game.associate = function(models) {
    // associations can be defined here
    const { team, game, week } = models;
    game.belongsTo(team, { foreignKey: 'home_team', as: 'home' });
    game.belongsTo(team, { foreignKey: 'away_team', as: 'away' });
    game.belongsTo(week, { foreignKey: 'week_played' });
  };
  return game;
};
