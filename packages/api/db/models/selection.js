'use strict';
module.exports = (sequelize, DataTypes) => {
  const selection = sequelize.define(
    'selection',
    {
      user_id: DataTypes.STRING,
      game_id: DataTypes.UUID,
      team_id: DataTypes.UUID,
    },
    {
      underscored: true,
      indexes: [
        {
          name: 'unique_selection',
          unique: true,
          fields: ['user_id', 'game_id'],
        },
      ],
    },
  );
  selection.associate = function(models) {
    // associations can be defined here
    const { team, game, user } = models;
    selection.belongsTo(user, { foreignKey: 'user_id' });
    selection.belongsTo(game, { foreignKey: 'game_id' });
    selection.belongsTo(team, { foreignKey: 'team_id' });
  };
  return selection;
};
