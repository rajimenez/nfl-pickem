'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define(
    'user',
    {
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
    },
    {
      underscored: true,
      timestamps: false,
    },
  );
  user.associate = function(models) {
    // associations can be defined here
  };
  return user;
};
