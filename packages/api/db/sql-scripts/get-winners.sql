select g.id,
       home_score,
       away_score,
       g.time_played,
       t.id as winner_team_id,
       t.name as winner_team_name,
       t.location as winner_team_location,
       t.short_name as winner_team_short_name
from
     (
         select id as id,
                home_score,
                away_score,
                time_played,
                case
                    when away_score > home_score then away_team
                    when home_score > away_score then home_team
                    else null
                    end as winner
         from games
         where time_played < NOW()
     ) as g
left join teams t on g.winner = t.id;
