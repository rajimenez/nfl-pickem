'use strict';
module.exports = (sequelize, DataTypes) => {
  const team = sequelize.define('team', {
    name: {
      unique: true,
      type: DataTypes.STRING,
    },
    location: DataTypes.STRING,
    short_name: {
      unique: true,
      type: DataTypes.STRING,
    }
  }, {
    underscored: true,
    timestamps: false,
  });
  team.associate = function(models) {
    // associations can be defined here
  };
  return team;
};
