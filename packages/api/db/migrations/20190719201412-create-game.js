'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('games', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal('gen_random_uuid()')
      },
      home_team: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'teams',
          }
        }
      },
      away_team: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'teams',
          }
        }
      },
      home_score: {
        type: Sequelize.INTEGER
      },
      away_score: {
        type: Sequelize.INTEGER
      },
      time_played: {
        type: Sequelize.DATE
      },
      week_played: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'weeks',
          }
        }
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('games');
  }
};
