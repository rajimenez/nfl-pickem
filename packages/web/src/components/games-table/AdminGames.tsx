import React from 'react';
import Grid from '@material-ui/core/Grid';
import useStyles from 'styles/styles';
import { Game } from 'components/games-table/Games';
import parseDate from 'date-fns/parseISO';
import getMonth from 'date-fns/getMonth';
import getDate from 'date-fns/getDate';
import getYear from 'date-fns/getYear';
import getHours from 'date-fns/getHours';
import getMinutes from 'date-fns/getMinutes';
import getDaysInMonth from 'date-fns/getDaysInMonth';
import { post } from 'api/Games';
import { useAuth0 } from 'react-auth0-wrapper';
import Button from '@material-ui/core/Button';
import WeekSelect from 'components/week-select/WeekSelect';
import Typography from '@material-ui/core/Typography';

interface Props {
  games: Game[];
  week: number;
  setWeek: (week: number) => void;
}

interface GameById {
  [id: string]: Game;
}

const twelveHourToTwentyFour = (hour: number, ampm: 'am' | 'pm') =>
  ampm === 'am' ? (hour === 12 ? 0 : hour) : hour === 12 ? hour : hour + 12;

const twentyFourHourToTwelve = (h24: number) => {
  const hour = h24 === 0 ? 12 : h24 > 12 ? h24 - 12 : h24;
  const ampm: 'am' | 'pm' = h24 < 12 ? 'am' : 'pm';
  return { hour, ampm };
};

const getTimeValues = (isoString?: string) => {
  const date = !!isoString ? parseDate(isoString) : new Date();
  const h24 = getHours(date);
  const { hour, ampm } = twentyFourHourToTwelve(h24);
  return {
    year: getYear(date),
    month: getMonth(date),
    date: getDate(date),
    hour,
    minute: getMinutes(date),
    ampm,
    second: 0,
  };
};

const mergeGame = (g: Game, modifiedGame?: Game) => {
  if (!modifiedGame) {
    return g;
  }
  return {
    AwayTeamLocation: modifiedGame.AwayTeamLocation || g.AwayTeamLocation,
    AwayTeamName: modifiedGame.AwayTeamName || g.AwayTeamName,
    AwayTeamShortName: modifiedGame.AwayTeamShortName || g.AwayTeamShortName,
    HomeTeamLocation: modifiedGame.HomeTeamLocation || g.HomeTeamLocation,
    HomeTeamName: modifiedGame.HomeTeamName || g.HomeTeamName,
    HomeTeamShortName: modifiedGame.HomeTeamShortName || g.HomeTeamShortName,
    AwayTeamScore: modifiedGame.AwayTeamScore || g.AwayTeamScore,
    HomeTeamScore: modifiedGame.HomeTeamScore || g.HomeTeamScore,
    TimePlayed: modifiedGame.TimePlayed || g.TimePlayed,
    UUID: g.UUID,
  };
};

function AdminGamesTable({ games, setWeek, week }: Props) {
  const [modifiedGames, setModifiedGames] = React.useState<GameById>({});
  const [edited, setEdited] = React.useState(false);
  const classes = useStyles();
  const { getTokenSilently } = useAuth0();

  const handleTimeChange = (gameID: string, date: Date) => {
    const timeObj = { TimePlayed: date.toISOString() };
    console.log(timeObj);
    const currentGame =
      modifiedGames[gameID] || games.find(g => g.UUID === gameID);

    const updatedGame = { ...currentGame, ...timeObj };
    setModifiedGames({ ...modifiedGames, [gameID]: updatedGame });
    setEdited(true);
  };

  const handleScoreChange = (
    gameID: string,
    homeAway: 'home' | 'away',
    score: number,
  ) => {
    if (Number.isNaN(score)) {
      return;
    }
    const scoreObj = {
      [homeAway === 'home' ? 'HomeTeamScore' : 'AwayTeamScore']: score,
    };
    const currentGame =
      modifiedGames[gameID] || games.find(g => g.UUID === gameID);

    const updatedGame = { ...currentGame, ...scoreObj };

    setModifiedGames({ ...modifiedGames, [gameID]: updatedGame });
    setEdited(true);
  };

  const handleSubmit = async () => {
    const token = await getTokenSilently();
    const data = await post(Object.values(modifiedGames), token);
    if (data.success) {
      window.location.reload();
    }
    console.log(data);
  };

  return (
    <>
      <Grid container>
        <Grid item xs={6}>
          {!edited && <WeekSelect value={week} onChange={setWeek} />}
          {edited && <Button onClick={handleSubmit}>Submit</Button>}
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={4}>
          TIME
        </Grid>
        <Grid item xs={4}>
          <Grid container>
            <Grid item xs={6}>
              AWAY
            </Grid>
            <Grid item xs={6}>
              SCORE
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={4}>
          <Grid container>
            <Grid item xs={6}>
              HOME
            </Grid>
            <Grid item xs={6}>
              SCORE
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {games.map(g => {
        const game = mergeGame(g, modifiedGames[g.UUID]);
        const { year, month, date, hour, minute, ampm } = getTimeValues(
          game.TimePlayed,
        );
        return (
          <Grid container key={game.UUID} className={classes.adminTableRow}>
            <Grid item xs={4}>
              <select
                value={month}
                onChange={e => {
                  const newMonth = Number(e.target.value);
                  const newHour = twelveHourToTwentyFour(hour, ampm);

                  handleTimeChange(
                    game.UUID,
                    new Date(year, newMonth, date, newHour, minute, 0, 0),
                  );
                }}
              >
                <option value={0}>Jan</option>
                <option value={1}>Feb</option>
                <option value={2}>Mar</option>
                <option value={3}>Apr</option>
                <option value={4}>May</option>
                <option value={5}>Jun</option>
                <option value={6}>Jul</option>
                <option value={7}>Aug</option>
                <option value={8}>Sep</option>
                <option value={9}>Oct</option>
                <option value={10}>Nov</option>
                <option value={11}>Dec</option>
              </select>
              <select
                value={date}
                onChange={e => {
                  const newDate = Number(e.target.value);
                  const newHour = twelveHourToTwentyFour(hour, ampm);
                  handleTimeChange(
                    game.UUID,
                    new Date(year, month, newDate, newHour, minute, 0, 0),
                  );
                }}
              >
                {Array.from(
                  { length: getDaysInMonth(new Date(year, month)) },
                  (_, i) => i + 1,
                ).map(d => (
                  <option value={d} key={d}>
                    {d}
                  </option>
                ))}
              </select>
              <select
                value={year}
                onChange={e => {
                  const newYear = Number(e.target.value);
                  const newHour = twelveHourToTwentyFour(hour, ampm);
                  handleTimeChange(
                    game.UUID,
                    new Date(newYear, month, date, newHour, minute, 0, 0),
                  );
                }}
              >
                <option value={2019}>2019</option>
                <option value={2020}>2020</option>
              </select>
              <select
                value={hour}
                onChange={e => {
                  const newHour = twelveHourToTwentyFour(
                    Number(e.target.value),
                    ampm,
                  );
                  handleTimeChange(
                    game.UUID,
                    new Date(year, month, date, newHour, minute, 0, 0),
                  );
                }}
              >
                {Array.from({ length: 12 }, (_, i) => i + 1).map(h => (
                  <option value={h} key={h}>
                    {h}
                  </option>
                ))}
              </select>
              <select
                value={minute}
                onChange={e => {
                  const newMinute = Number(e.target.value);
                  const newHour = twelveHourToTwentyFour(hour, ampm);
                  handleTimeChange(
                    game.UUID,
                    new Date(year, month, date, newHour, newMinute, 0, 0),
                  );
                }}
              >
                {Array.from({ length: 60 }, (_, i) => i).map(h => (
                  <option value={h} key={h}>
                    {h}
                  </option>
                ))}
              </select>
              <select
                value={ampm}
                onChange={e => {
                  const newHour = twelveHourToTwentyFour(hour, e.target
                    .value as 'am' | 'pm');
                  handleTimeChange(
                    game.UUID,
                    new Date(year, month, date, newHour, minute, 0, 0),
                  );
                }}
              >
                <option value="am">am</option>
                <option value="pm">pm</option>
              </select>
            </Grid>
            <Grid item xs={4}>
              <Grid container>
                <Grid item xs={6}>
                  {game.AwayTeamName}
                </Grid>
                <Grid item xs={6}>
                  <input
                    type="text"
                    value={game.AwayTeamScore || ''}
                    onChange={e =>
                      handleScoreChange(
                        game.UUID,
                        'away',
                        Number(e.target.value),
                      )
                    }
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={4}>
              <Grid container>
                <Grid item xs={6}>
                  {game.HomeTeamName}
                </Grid>
                <Grid item xs={6}>
                  <input
                    type="text"
                    value={game.HomeTeamScore || ''}
                    onChange={e =>
                      handleScoreChange(
                        game.UUID,
                        'home',
                        Number(e.target.value),
                      )
                    }
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        );
      })}
    </>
  );
}

export default AdminGamesTable;
