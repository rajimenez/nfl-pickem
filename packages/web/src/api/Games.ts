import axios from 'axios';
import { Game } from 'components/games-table/Games';

export const get = async (week: number, token: string) => {
  const { data } = await axios.get(`http://localhost:8080/games?week=${week}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const games: Game[] = data.map((g: any) => ({
    UUID: g.id,
    AwayTeamLocation: g.away.location,
    AwayTeamName: g.away.name,
    AwayTeamShortName: g.away.short_name,
    HomeTeamLocation: g.home.location,
    HomeTeamName: g.home.name,
    HomeTeamShortName: g.home.short_name,
    HomeTeamScore: g.home_score,
    AwayTeamScore: g.away_score,
    HomeID: g.home_team,
    AwayID: g.away_team,
    TimePlayed: g.time_played,
  }));

  return games;
};

export const post = async (games: Game[], token: string) => {
  const dbGames = games.map(g => ({
    id: g.UUID,
    time_played: g.TimePlayed,
    home_score: g.HomeTeamScore,
    away_score: g.AwayTeamScore,
  }));
  const { data } = await axios.post('http://localhost:8080/games', dbGames, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
};
