import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import useStyles from 'styles/styles';
import Button from '@material-ui/core/Button';
import { useAuth0 } from 'react-auth0-wrapper';

export default () => {
  const classes = useStyles();
  const { loginWithRedirect } = useAuth0();
  return (
    <>
      <Typography variant="h4" className={classes.pageTitle}>
        Login
      </Typography>
      <Paper className={classes.loginCard}>
        <Typography variant="h6">
          You must{' '}
          <Button
            variant="contained"
            color="primary"
            className={classes.loginButton}
            onClick={() => loginWithRedirect()}
          >
            Login
          </Button>{' '}
          to continue.
        </Typography>
      </Paper>
    </>
  );
};
