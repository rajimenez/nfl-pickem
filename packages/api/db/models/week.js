'use strict';
module.exports = (sequelize, DataTypes) => {
  const week = sequelize.define('week', {
    week: DataTypes.INTEGER,
    year: DataTypes.INTEGER
  }, {
    underscored: true,
    indexes: [
      {
        name: 'unique_week',
        unique: true,
        fields: [ 'week', 'year' ],
      },
    ],
    timestamps: false,
  });
  week.associate = function(models) {
    // associations can be defined here
  };
  return week;
};
