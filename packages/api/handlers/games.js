const db = require('../db/models/index');
const { team, game, week } = db;

const gamesHandler = async (req, res) => {
  const { week: weekQ, year: yearQ } = req.query;
  if (weekQ) {
    const year = yearQ ? yearQ : new Date().getFullYear();
    res.json(
      await game.findAll({
        include: [
          { model: team, foreignKey: 'home_team', as: 'home' },
          { model: team, foreignKey: 'away_team', as: 'away' },
          { model: week, where: { week: weekQ, year } },
        ],
        order: ['time_played'],
      }),
    );
  } else if (yearQ) {
    const weeks = await week.findAll({ where: { year: yearQ } });
    const weekIDs = weeks.map(w => w.id);
    res.json(
      await game.findAll({
        where: { week_played: weekIDs },
        order: ['time_played'],
      }),
    );
  } else {
    res.json(
      await game.findAll({
        include: [
          { model: team, foreignKey: 'home_team', as: 'home' },
          {
            model: team,
            foreignKey: 'away_team',
            as: 'away',
          },
          week,
        ],
        order: ['time_played'],
      }),
    );
  }
};

module.exports = gamesHandler;
